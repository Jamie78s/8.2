class_name Player extends CharacterBody3D

var health = 100






@export_category("Player")
@export_range(1, 35, 1) var speed: float = 10 # m/s
@export_range(10, 400, 1) var acceleration: float = 100 # m/s^2

@export_range(0.1, 3.0, 0.1) var jump_height: float = 1 # m
@export_range(0.1, 9.25, 0.05, "or_greater") var camera_sens: float = 3

var jumping: bool = false
var mouse_captured: bool = false

var attack_animation = [
	"attack1",
	"attack2",
	"attack3",
]

var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

var move_dir: Vector2 # Input direction for movement
var look_dir: Vector2 # Input direction for look/aim

var walk_vel: Vector3 # Walking velocity 
var grav_vel: Vector3 # Gravity velocity 
var jump_vel: Vector3 # Jumping velocity

@onready var camera: Camera3D = $Camera

func _ready() -> void:
	capture_mouse()

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion: look_dir = event.relative * 0.01
	if Input.is_action_just_pressed("jump"): jumping = true
	if Input.is_action_just_pressed("exit"): get_tree().quit()


	if Input.is_action_just_pressed("attack"):
		do_damage()
		$Control/AnimationPlayer.play(attack_animation.pick_random())
		
	if Input.is_action_pressed("block"):
		$Control/block_animation.play("block")
	elif !Input.is_action_pressed("block"):
		$Control/block_animation.play("RESET")




func _physics_process(delta: float) -> void:
	Global.player_pos = global_position
	if mouse_captured: _rotate_camera(delta)
	velocity = _walk(delta) + _gravity(delta) + _jump(delta)
	move_and_slide()
	$Control/MarginContainer/VBoxContainer2/Label.text = str("kills: " + str(Global.kills))
	
	$Control/MarginContainer/VBoxContainer/TextureProgressBar.value = health
	$Control/MarginContainer/VBoxContainer/TextureProgressBar/Label.text = str(str(health) + "/100")

func capture_mouse() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	mouse_captured = true

func release_mouse() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	mouse_captured = false

func _rotate_camera(delta: float, sens_mod: float = 1.0) -> void:
	look_dir += Input.get_vector("look_left","look_right","look_up","look_down")
	camera.rotation.y -= look_dir.x * camera_sens * sens_mod * delta
	camera.rotation.x = clamp(camera.rotation.x - look_dir.y * camera_sens * sens_mod * delta, -1.5, 1.5)
	look_dir = Vector2.ZERO

func _walk(delta: float) -> Vector3:
	move_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backwards")
	var _forward: Vector3 = camera.transform.basis * Vector3(move_dir.x, 0, move_dir.y)
	var walk_dir: Vector3 = Vector3(_forward.x, 0, _forward.z).normalized()
	walk_vel = walk_vel.move_toward(walk_dir * speed * move_dir.length(), acceleration * delta)
	return walk_vel

func _gravity(delta: float) -> Vector3:
	grav_vel = Vector3.ZERO if is_on_floor() else grav_vel.move_toward(Vector3(0, velocity.y - gravity, 0), gravity * delta)
	return grav_vel

func _jump(delta: float) -> Vector3:
	if jumping:
		if is_on_floor(): jump_vel = Vector3(0, sqrt(4 * jump_height * gravity), 0)
		jumping = false
		return jump_vel
	jump_vel = Vector3.ZERO if is_on_floor() else jump_vel.move_toward(Vector3.ZERO, gravity * delta)
	return jump_vel


func damage(amount):
	health = health - amount
	if health <= 0:
		$Control/AnimationPlayer.play("death")
		await get_tree().create_timer(1).timeout
		get_tree().change_scene_to_file("res://Scenes/main_menu.tscn")
		


func do_damage():
	$Camera/Area3D.monitoring = true
	await get_tree().create_timer(0.1).timeout
	$Camera/Area3D.monitoring = false


func _on_area_3d_body_entered(body):
	if body.is_in_group("enemy") and $Camera/Area3D.monitoring == true:
		var enemy = body
		body.damage(10)
