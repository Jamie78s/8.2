extends CharacterBody3D

@onready var health_texture = preload("res://enemy/enemy_text.tscn")

@onready var nav_agent = $NavigationAgent3D


var damage_dealt = 5

var health

var max_health

var player_near = false

var SPEED = 8

func _on_area_3d_body_entered(body):
	if $Area3D.monitoring == true and body.is_in_group("player"):
		damage_dealt = (Global.difficulty * damage_dealt)
		var player 
		player = body
		player.damage(damage_dealt)

func _ready():
	health_texture = health_texture.instantiate()
	add_child(health_texture)
	health_texture.position = Vector3(0,4,0)
	$Timer.start()
	max_health = randf_range(50, 100)
	max_health = (Global.difficulty * max_health)
	max_health = round(max_health)
	health = max_health


func _on_timer_timeout():
	$Area3D.monitoring = true
	await get_tree().create_timer(0.1).timeout
	$Area3D.monitoring = false

func update_target_location(target_location):
	$NavigationAgent3D.set_target_position(target_location)

func _physics_process(delta):
	look_at(Vector3(Global.player_pos.x, Global.player_pos.y, Global.player_pos.z))
	rotate_object_local(Vector3.UP, PI)
	rotation_degrees.x = 0
	var current_location = global_transform.origin
	var next_location = nav_agent.get_next_path_position()
	var new_velocity = (next_location - current_location).normalized() * SPEED
	velocity = new_velocity
	move_and_slide()



	$enemy_text/SubViewport/TextureProgressBar.value = health
	$enemy_text/SubViewport/TextureProgressBar.max_value = max_health
	$enemy_text/SubViewport/TextureProgressBar/Label.text = str(str(health) + "/" + str(max_health))

func damage(amount):
	health = health - amount
	if health <= 0:
		self.queue_free()
		Global.kills += 1
