extends Node3D

@onready var player = $Player

var random_time
var enemies_to_kill
var enemies_to_spawn
var can_spawn = false

func _ready():
	random_spawn()
	enemies_to_kill = 5

func random_spawn():
	enemies_to_spawn = randf_range(5, 10)
	enemies_to_spawn = (enemies_to_spawn * Global.difficulty)
	enemies_to_spawn = round(enemies_to_spawn)
	print(enemies_to_spawn)

func _physics_process(delta):
	if can_spawn == true:
		can_spawn = false
		_spawn(enemies_to_spawn)
	get_tree().call_group("enemy", "update_target_location", player.global_transform.origin)
	if Global.kills == enemies_to_kill:
		random_spawn()
		can_spawn = true
		enemies_to_kill += enemies_to_spawn
		Global.difficulty += 0.3

func _spawn(amount):
	for i in amount:
		await get_tree().create_timer(1).timeout
		var enemie = preload("res://enemy/enemy.tscn")
		enemie = enemie.instantiate()
		add_child(enemie)
		enemie.global_position = $spawnpos.global_position
	
