extends Control

func _ready():
	Global.kills = 0
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Global.difficulty = 1

func _on_settings_pressed():
	$"MarginContainer/play-menu".hide()
	$"MarginContainer/settings-menu".show()



func _on_back_pressed():
	$"MarginContainer/settings-menu".hide()
	$"MarginContainer/play-menu".show()


func _on_quit_pressed():
	get_tree().quit()


func _on_play_pressed():
	$MarginContainer.hide()
	$AnimationPlayer.play("camera-walk-up")
	await get_tree().create_timer(3.5).timeout
	get_tree().change_scene_to_file("res://Scenes/Throne_room.tscn")
	
